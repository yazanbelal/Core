<?php
namespace Core\Http;
/**
 * Class Response
 * @package PhpCore\Http
 * @property Response response()
 */
class Response {
	/**
	 * @var mixed PhpCore Http Response Body
	 */
	public $body;
	/**
	 * @var array PhpCore Http Response Header [name => value]
	 */
	private $headers = array();
	/**
	 * @var array PhpCore Http Response Cookies [name => value]
	 */
	public $cookies = array();
	/**
	 * @var int PhpCore Http Response Status Code
	 */
	private $status = 200;
	/**
	 * @param string $body
	 * @param bool $append
	 */
	public function body($body, $append=false){
		if($append) {
			$this->body .= $body;
		}else{
			$this->clean();
			$this->body = $body;
		}
	}
	public function clean(){
		ob_clean();
	}

	/**
	 * @param int $code
	 */
	public function status($code=200){
		$this->status = $code;
	}
	/**
	 * @param $name
	 * @param $value
	 * @function HTTP Headers
	 */
	public function header($name = null, $value = null){
		if(is_array($name) && !$value){
			foreach($name as $key => $value){
				$this->headers[$key] = $value;
			}
		}elseif(is_string($name) && is_string($value)){
			$this->headers[$name] = $value;
		}
	}
	public function write($data, $clean=false){
		if($clean) {
			$this->clean();
			$fp = fopen('php://output', 'w');
			fwrite($fp, $data);
			fclose($fp);
		}else{
			$fp = fopen('php://output', 'w');
			fwrite($fp, $data);
			fclose($fp);
		}
	}
	public function execute(){
		if(!empty($this->headers)){
			header_remove();
			foreach($this->headers as $key => $value){
				$header = $key.': '.$value;
				header($header);
			}
		}
		if($this->status != 200){
			http_response_code($this->status);
		}
		if(is_object($this->body) || is_array($this->body)){
			$this->write(print_r($this->body, true));
		}else{
			$this->write($this->body);
		}
	}
}