<?php
namespace Core\Http;

/**
 * Class Request
 * @package PhpCore
 */
class Request {
    /**
     * @var array Http SERVER data
     */
    private $server = array();
    /**
     * @var array Http Request headers
     */
    private $headers = array();
	/**
	 * @var array PhpCore Env Headers
	 */
	private $env = array();

    public function __construct(){
	    $env = array_intersect_key($_SERVER, array(
		    'HTTP_HOST' => '',
		    'HTTP_USER_AGENT' => '',
		    'HTTP_ACCEPT' => '',
		    'SERVER_NAME' => '',
		    'SERVER_PROTOCOL' => '',
		    'SERVER_ADDR' => '',
		    'REMOTE_ADDR' => '',
		    'REQUEST_SCHEME' => '',
		    'SCRIPT_NAME' => '',
		    'REQUEST_METHOD' => '',
		    'PATH_INFO' => '',
		    'REQUEST_URI' => ''
	    ));
	    $this->env = $env;
        $this->headers = getallheaders();
    }

	/**
	 * @param $name
	 * @return bool|mixed
	 */
	public function getServer($name){
		return isset($_SERVER[$name]) ? $_SERVER[$name] : false;
	}
	/**
	 * @return array Http Request Headers
	 */
    public function getHeaders(){
        return $this->headers;
    }

	/**
	 * @return string Http Request Method
	 */
    public function getMethod(){
        return isset($this->env['REQUEST_METHOD']) ? $this->env['REQUEST_METHOD'] : 'GET';
    }

	/**
	 * @return string Http Server Path Info
	 */
    public function getPathInfo(){
        return isset($this->env['PATH_INFO']) ? $this->env['PATH_INFO'] : '/';
    }

	/**
	 * @return string Http Client Ip
	 */
    public function getClientIp(){
        return isset($this->env['REMOTE_ADDR']) ? $this->env['REMOTE_ADDR'] : '127.0.0.1';
    }

	/**
	 * @return string Http Query String
	 */
    public function getQueryString(){
        return isset($this->env['QUERY_STRING']) ? $this->env['QUERY_STRING'] : 'no-query';
    }

	/**
	 * @return string Http Server IP
	 */
    public function getServerIp(){
        return isset($this->env['SERVER_ADDR']) ? $this->env['SERVER_ADDR'] : '127.0.0.1';
    }

	/**
	 * @return mixed Return $_SERVER REQUEST URI Content
	 */
	public function getRequestUri()
	{
		if (preg_match('#\?#', $this->env['REQUEST_URI'])) {
			$uri = explode('?', $this->env['REQUEST_URI']);
			$uri = $uri[0];
		}
		return isset($uri) ? $uri : $this->env['REQUEST_URI'];
	}

	/**
	 * @param string $name
	 * @return null
	 */
	public function fetchGet($name){
		return isset($_GET[$name]) ? $_GET[$name] : null;
	}
	public function fetchPost($name){
		return isset($_POST[$name]) ? $_POST[$name] : null;
	}
	public function fetchCookie($name){
		return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
	}
	public function fetchSession($name)
	{
		return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
	}
	/**
	 * @return bool|string
	 */
	public function getRawPostData(){
		$raw_data = file_get_contents('php://input');
		return !!($raw_data) ? $raw_data : false;
	}
}