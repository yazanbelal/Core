<?php
namespace Core\Exception;
/**
 * Class Main
 * @package PhpCore\Exception
 */
class Error extends \Exception{

    public function __construct($message = '', $code = 0){
        parent::__construct($message, $code);
    }
}