<?php
namespace Core;

/**
 * Class Container
 * @package PhpCore
 */
class Container{
	public $data = array();

	/**
	 * @param string $name
	 * @param mixed $value
	 */
	public function set($name, $value){
		$this->data[$name] = $value;
	}
	public function get($name){
		if($this->has($name)){
			$name = $this->data[$name];
			return $name;
		}
		throw new Exception\Error('not found');
	}
	public function has($name){
		return array_key_exists($name, $this->data);
	}
}