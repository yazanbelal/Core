<?php
/**
 * Core Framework for PHP
 * @author Mohamed Belal
 * @version 1.0.0
 *
 * @property Core\Http\Response $response
 * @property Core\Http\Request  $request
 * @property Core\Router        $router
 */
class Core
{
    /**
     * @version Core Framework Version
     */
    const VERSION = '1.0';
	/**
	 * @var Core\Container
	 */
	public $container;
    /**
     * @var array PhpCore Default Config Array
     */
    private $defaultConfig = array();
    /**
     * @var string|callable PhpCore Default Error String or Function
     */
    private  $error;
	/**
	 * @var object PhpCore Vars
	 */
	public $vars = array();

    /**
     * @param array $userConfig
     */
    public function __construct(array $userConfig=array())
    {
	    $this->defaultConfig = array(
		    'debug' => true,
		    'mode' => 'development',
		    'mysql' => false,
		    'parser' => false,
		    'http' => false
	    );
	    $this->defaultConfig = isset($userConfig) ? array_merge($this->defaultConfig, $userConfig) : $this->defaultConfig;
	    //PhpCore Container
	    $this->container = new Core\Container();
	    $this->container->set('phpcore', $this);
	    $this->container->set('router', new Core\Router($this));
	    $this->container->set('request', new Core\Http\Request());
	    $this->container->set('response', new Core\Http\Response());
	    //($this->defaultConfig['mysql']) ? $this->container->set('mysql', new PhpCore\Helper\MySQL()) : null;
	    //($this->defaultConfig['parser']) ? $this->container->set('parser', new PhpCore\Helper\Parser()) : null;
	    //($this->defaultConfig['http']) ? $this->container->set('http', new PhpCore\Helper\Http()) : null;
	    /*
	    if($this->config['phpcore.helper.mysql']){
		    $this->container->set('mysql', new PhpCore\Helper\MySQL());
	    }elseif($this->config['phpcore.helper.parser']){
		    $this->container->set('parser', new PhpCore\Helper\Parser());
	    }elseif($this->config['phpcore.helper.http']){
		    $this->container->set('http', new PhpCore\Helper\Http());
	    }*/

    }

	/**
	 * @param $name
	 * @return mixed
	 * @throws Core\Exception\Error
	 */
	public function __get($name){
		return $this->container->get($name);
	}

	/**
	 * @param mixed $name
	 * @param mixed $value
	 */
	public function __set($name, $value){
		$this->container->set($name, $value);
	}

	/**
	 * @param mixed $name
	 * @param mixed $value
	 */
	public function set($name, $value){
		static::__set($name, $value);
		//$this->container->set($name, $value);
	}
    public function error(){
        $args = func_get_args();
        if(is_callable($args[0]) and !is_string($args[0])){
            $this->error = $args[0];
        }else{
            $this->response->status(500);
            $this->response->write($this->invokeError($args), true);
	        //$this->stop();
        }
    }
	public function call($function, $args=array()){
		ob_start();
		if(is_callable($function)){
			call_user_func_array($function, $args);
		}
		return ob_get_clean();
	}
	public function stop(){
		throw new Core\Exception\Error;
	}
    protected function invokeError($args){
        ob_start();
        if(is_callable($this->error)){
            call_user_func_array($this->error, $args);
        }else{
            call_user_func(array($this, 'defaultError'));
        }
        return ob_get_clean();
    }
    private function setErrorHandler(){
        set_error_handler(array($this, 'errorHandler'), E_ALL);
    }
    public function getFrameworkName(){
        return "Core Framework ".self::VERSION;
    }
    public function config($name, $value = ''){
        $config = array();
        if(is_array($name) && !$value){
            foreach($name as $key => $value){
                $config[$key] = $value;
            }
            $this->defaultConfig = array_merge($this->defaultConfig, $config);
            return true;
        }elseif(is_string($name) && is_string($value)){
            $this->defaultConfig[$name] = $value;
            return true;
        }else{
            return false;
        }
    }
	public function before($method, $callable, $args=array()){
		$this->router->before($method, $callable, $args);
	}
    /**
     * @param string $pattern
     * @param callable $callback
     */
    public function get($pattern, $callback)
    {
        $this->router->addRoute(array('GET'), $pattern, $callback);
    }

    /**
     * @param string $pattern
     * @param callable $callback
     */
    public function post($pattern, callable $callback)
    {
        $this->router->addRoute(array('POST'), $pattern, $callback);
    }

    public function run()
    {
        try{
            $this->setErrorHandler();
            if(!$this->router->getRoutes()){
	            $this->router->defaultRoute();
            }
	        $this->router->execute();
	        $this->response->execute();
            restore_error_handler();
        } catch(Core\Exception\Error $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param string $type
     * @return bool|string
     */
    public function contentType($type){
        if($type){
            $this->response->header('Content-Type', $type);
            return true;
        }
        return false;
    }

    /**
     * @param $className
     */
    static function autoload($className)
    {
        $thisClass = str_replace(__NAMESPACE__.'\\', '', __CLASS__);
        $baseDir = __DIR__;
        if (substr($baseDir, -strlen($thisClass)) === $thisClass) {
            $baseDir = substr($baseDir, 0, -strlen($thisClass));
        }
        $className = ltrim($className, '\\');
        $fileName  = $baseDir;
        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  .= str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
        if (file_exists($fileName)) {
            require $fileName;
        }
    }
    static function registerAutoLoader(){
        spl_autoload_register(array('Core', 'autoload'));
    }

	/**
	 * @param int $errno
	 * @param string $errstr
	 * @param string $file
	 * @param int $line
	 * @throws Core\Exception\Error
	 */
    public function errorHandler($errno = 0, $errstr = '', $file = '', $line = 0){
        $html = '';
        switch($this->defaultConfig['mode']){
            case 'development':
                $html = '<!DOCTYPE html><html><head lang=en><meta charset=UTF-8><title>PhpCore Error</title><style>body{margin:0;padding:30px;font:12px /1.5 Helvetica,Arial,Verdana,sans-serif}h1{margin:0;font-size:48px;font-weight:normal;line-height:48px}strong{display:inline-block;width:65px}</style></head><body><h1>PhpCore Error</h1><h2>Details</h2><br/><strong>Type:</strong><b>{type}</b><br/><strong>Code:</strong><b>{code}</b><br/><strong>File:</strong><b>{file}</b><br/><strong>Line:</strong><b>{line}</b></body></html>';
                $html = str_replace(array(
                    '{type}',
                    '{code}',
                    '{file}',
                    '{line}'
                ), array(
                    $errstr,
                    $errno,
                    $file,
                    $line
                ), $html);
                $this->response->status(500);
                break;
            case 'production':
                $html = $this->error;
                $this->response->status(500);
                break;
        }
        throw new Core\Exception\Error($html);
    }

    /**
     * @param callable $function Function to Be Invoked
     * @param string $args Function Args if available
     * @return string return Buffer
     */
    protected function invoke(callable $function, $args){
        ob_start();
        if(is_callable($function) and isset($args)){
            call_user_func_array($function, $args);
        }
        return ob_get_clean();
    }
    protected static function generateTemplateMarkup($title, $body)
    {
        return sprintf("<!DOCTYPE html><html><head><title>%s</title><style>body{text-align center;margin:0;padding:30px;font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;}h1{text-align: center;margin:0;font-size:42px;font-weight:normal;line-height:40px;}strong{display:inline-block;width:65px;}*{text-align: center;}</style></head><body><h1>%s</h1>%s</body></html>", $title, $title, $body);
    }
    protected function defaultError()
    {
        echo self::generateTemplateMarkup('Error', '<p>A website error has occurred. Sorry for the temporary inconvenience.</p>');
    }
}