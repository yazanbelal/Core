<?php
namespace Core;
/**
 * Class Router
 * @package PhpCore
 */
class Router{
	/**
	 * @var array HTTP Routes
	 */
	private $routes = array();
	/**
	 * @var array Callbacks before Routes
	 */
	private $befores = array();
	/**
	 * @var array Callbacks After Routes
	 */
	private $afters = array();
	/**
	 * @var \Core Parent Class Object
	 */
	private $parent;

	public function __construct(\Core $parent){
		$this->parent = $parent;
	}

	/**
	 * @param array $method
	 * @param string $pattern
	 * @param mixed $callable
	 */
	public function addRoute(array $method, $pattern, $callable)
	{
		$this->routes[] = new Route($method, $pattern, $callable);
	}
	public function before($method, $callable, $args=null){
		if(is_array($method)){
			foreach($method as $m){
				$this->befores[$m][] = ['callable' => $callable, 'args' => $args];
			}
		}else{
			$this->befores[$method][] = ['callable' => $callable, 'args' => $args];
		}
	}

	/**
	 * Register Default Route if no Routes Defined
	 */
	public function defaultRoute(){
		$this->routes[] = new Route(array('GET'), '/', function(){echo "Default HTTP Route";});
	}

	/**
	 * @return array Get Defined Http Routes
	 */
	public function getRoutes(){
		return $this->routes;
	}
	public function execute(){
		$pathInfo = $this->parent->request->getPathInfo();
		$requestMethod = $this->parent->request->getMethod();
		if($this->befores) {
			foreach ($this->befores as $key => $value) {
				if ($requestMethod == $key) {
					foreach ($value as $v) {
						$this->parent->response->body($this->call($v['callable'], $v['args']));
					}
				}
			}
		}
		$match = false;
		foreach($this->routes as $route) {
			if (preg_match($route->getPatternRegex(), $pathInfo, $matches) && in_array($requestMethod, $route->getMethod())) {
				$match = true;
				$params = array_intersect_key(
					$matches,
					array_flip(array_filter(array_keys($matches), 'is_string'))
				);
				$this->parent->response->body($this->parent->call($route->getCallable(), array_values($params)));
			} else {
				continue;
			}
		}
		if(!$match){
			throw new Exception\Error('404 not found');
		}
	}
	private function call($function, $args=null){
		ob_start();
		if(is_callable($function)){
			call_user_func_array($function, $args);
		}
		return ob_get_clean();
	}
}