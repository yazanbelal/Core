<?php
namespace Core;
/**
 * Class Route
 * @package PhpCore
 */
class Route {
	/**
	 * @var array Route HTTP Method
	 */
	private $method;
	/**
	 * @var string Route Pattern
	 */
	private $pattern;
	/**
	 * @var mixed Route Callable
	 */
	private $callable;
	/**
	 * @var string Route Regex
	 */
	private $regex;
	/**
	 * @param string $method
	 * @param string $pattern
	 * @param mixed $callable
	 * @throws Exception\Error
	 */
	public function __construct($method, $pattern, $callable){
		$this->setMethod($method);
		$this->setPatternRegex($pattern);
		$this->setPattern($pattern);
		$this->setCallable($callable);
	}
	public function setPatternRegex($pattern){
		$pattern = str_replace(')', ')?', $pattern);
		$allowedParamChars = '[\w\W]+';
		$pattern = preg_replace(
			'/:(' . $allowedParamChars . ')/',
			'(?<$1>' . $allowedParamChars . ')',
			$pattern
		);
		$pattern = preg_replace(
			'/{('. $allowedParamChars .')}/',
			'(?<$1>' . $allowedParamChars . ')',
			$pattern
		);
		$patternAsRegex = "@^" . $pattern . "$@D";
		$this->regex = $patternAsRegex;
	}
	public function getPatternRegex(){
		return $this->regex;
	}
	public function setCallable($callable){
		$this->callable = $callable;
	}
	public function setPattern($pattern){
		if (preg_match('/[^-:\/_{}()a-zA-Z\d]/', $pattern)){
			throw new Exception\Error('Invalid Pattern');
		}
		$this->pattern = $pattern;
	}
	public function getPattern()
	{
		return $this->pattern;
	}
	public function getCallable(){
		return $this->callable;
	}
	public function setMethod($method){
		$this->method = $method;
	}
	public function getMethod(){
		return $this->method;
	}
}